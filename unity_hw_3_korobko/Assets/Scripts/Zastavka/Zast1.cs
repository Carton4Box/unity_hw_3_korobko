﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zast1 : MonoBehaviour
{
    public bool isLoad = false;
    public GameObject B_Game0;
    public GameObject B_Game1;
    public GameObject B_Game2;

    public float SpeedF;
    private float count;

    void Update()
    {
        if(isLoad == true)
        {
            gameObject.GetComponent<Animator>().enabled = false;
            B_Game0.SetActive(true);
            B_Game1.SetActive(true);
            B_Game2.SetActive(true);
            count += SpeedF * Time.deltaTime;
            if (B_Game0.GetComponent<Image>().color.a < 255)
            {
                B_Game0.GetComponent<Image>().color = new Color(255f, 255f, 255f, B_Game0.GetComponent<Image>().color.a + count);
                B_Game1.GetComponent<Image>().color = new Color(255f, 255f, 255f, B_Game1.GetComponent<Image>().color.a + count);
                B_Game2.GetComponent<Image>().color = new Color(255f, 255f, 255f, B_Game2.GetComponent<Image>().color.a + count);
            }
        } 
    }
}
