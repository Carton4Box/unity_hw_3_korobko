﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class play_game0 : MonoBehaviour
{
    public string sceneName;
    public Button B_Game0;

    // Start is called before the first frame update
    void Start()
    {
        B_Game0.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene(sceneName);
    }
}
