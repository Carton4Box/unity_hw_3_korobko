﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class play_game2 : MonoBehaviour
{
    public string sceneName;
    public Button B_Game2;

    // Start is called before the first frame update
    void Start()
    {
        B_Game2.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene(sceneName);
    }
}
