using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game2
{

public class UIAbility : MonoBehaviour
{
  public Text name_text;
  public Text description_text;
  Unit.Ability ability;
  Action<Unit.Ability> on_select;
  Button button;

  void Awake()
  {
    button = GetComponent<Button>();
  }

  public void Init(Unit.Ability ability, Action<Unit.Ability> on_select)
  {
    this.ability = ability;
    this.on_select = on_select;
    name_text.text = ability.name;
    button.onClick.AddListener(OnClick);
  }

  void OnClick()
  {
    if(ability.cooldown_timer != 0)
      return;

    on_select?.Invoke(ability);
  }

  void Update()
  {
    if(ability == null)
      return;

    button.interactable = ability.cooldown_timer == 0;

    if(button.interactable)
    {
      if(ability.target_type == Unit.Ability.TargetType.ENEMY)
        description_text.text = $"Урон: {ability.damage}";
      else
        description_text.text = $"Здоровье: +{-ability.damage}";
    }
    else
    {
      description_text.text = $"До зарядки: {ability.cooldown_timer}";
    }
  }
}

}
