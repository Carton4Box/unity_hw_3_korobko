﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game2
{

public class Unit : MonoBehaviour
{
  [System.Serializable]
  public class Ability
  {
    public enum TargetType
    {
      ENEMY,
      SELF
    }

    public string name;
    public float damage;
    public int cooldown;
    public TargetType target_type;

    [System.NonSerialized]
    public int cooldown_timer = 0;

    public string animation_trigger;
  }

  [System.NonSerialized]
  public float hp;
  public float max_hp;

  public List<Ability> abilities;

  Animator animator;

  void Awake()
  {
    animator = GetComponent<Animator>();
    hp = max_hp;
  }

  public void Hit(in Ability ability)
  {
    hp = Mathf.Clamp(hp - ability.damage, 0, max_hp) ;
  }

  public void UseAbility(Ability ability)
  {
    ability.cooldown_timer = ability.cooldown + 1; //NOTE: Add +1 for current turn
  }

  public void OnTurnStart()
  {
    foreach(var ability in abilities)
    {
      if(ability.cooldown_timer == 0)
        continue;

      ability.cooldown_timer--;
    }
  }

  public void LookAtTarget(Unit other)
  {
    transform.forward = (other.transform.position - transform.position).normalized;
  }


  public bool IsAlive()
  {
    return hp > 0;
  }

  public void StartAnimation(string trigger_name)
  {
    animator.SetTrigger(trigger_name);
  }
}

}
